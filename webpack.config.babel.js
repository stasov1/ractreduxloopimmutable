/* eslint-disable */
'use strict';

import webpack from 'webpack';
import path from 'path';

import ExtractTextPlugin from 'extract-text-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const NODE_ENV = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';

const config = {
  context: path.join(__dirname, '/src/'),

  entry: {
    index: ['babel-polyfill', './index']
  },

  output: {
    path: path.join(__dirname, 'build'),
    publicPath: '/',
    filename: 'bundle.js',
  },

  resolve: {
    root: [path.resolve('node_modules')],
    modulesDirectories: [path.resolve('node_modules')],
    extensions: ['', '.web.js', '.js', '.jsx', '.json']
  },

  resolveLoader: {
    root: [path.resolve('node_modules')],
    modulesDirectories: [path.resolve('node_modules')]
  },

  devtool: NODE_ENV == 'development' ? 'eval' : false,

  module: {
    preLoaders: [
      {
        test: /\.jsx?$/,
        loader: 'eslint-loader',
        exclude: /node_modules/,
        include: __dirname
      }
    ],
    loaders: [
      {
        test: /\.jsx?$/,
        loaders: ['react-hot-loader/webpack', 'babel-loader'],
        exclude: /node_modules/,
        include: __dirname
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style', `css?module&importLoaders=1&localIdentName=${NODE_ENV == 'development' ? '[name]__[local]__[hash:base64:5]' : '[hash:base64:5]'}!postcss-loader`)
      },
      {
        test: /\.(ttf|eot|svg|woff2?)(\?[a-z0-9]+)$/,
        loader: 'file?name=[path][name].[ext]'
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'url?limit=1000&name=[path][name].[ext]?[hash:base64:5]!image-webpack?{progressive: true, optimizationLevel: 7, interlaced: false, pngquant: {quality: "65-90", speen: 4}}',
        exclude: /node_modules/
      },
      {
        test: /\.(mp4|webm)$/,
        loader: 'file?name=[path][name].[ext]'
      }
    ]
  },

  devServer: {
    contentBase: path.join(__dirname, 'build'),
    hot: true,
    inline: true,
    host: 'localhost',
    port: 9092,
    historyApiFallback: true,
    stats: {
      colors: true
    },
    // proxy: {
    //   '/api/*': {
    //     target: 'http://yourSite',
    //     changeOrigin: true,
    //     secure: false
    //   }
    // }
  },

  plugins: [
    new ExtractTextPlugin('css/[name].css'),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(NODE_ENV)
    }),
    new HtmlWebpackPlugin({
      template: 'index.ejs',
      inject: 'body',
      hash: true
    })
  ],

  eslint: {
    configFile: './.eslintrc'
  },

  postcss: function() {
    return [
      require('precss'),
      require('postcss-em-media-query'),
      require('postcss-pxtorem')({
        rootValue: 16
      }),
      require('postcss-clearfix'),
      require('postcss-color-function'),
      require('postcss-color-alpha'),
      require('autoprefixer')
    ];
  }
}

if (NODE_ENV === 'production') {
  config.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: false
    })
  )
}

if (NODE_ENV === 'development') {
  config.plugins.push(
    new webpack.HotModuleReplacementPlugin()
  )
}

export default config;

/* eslint-enable */
