import { combineReducers } from 'redux-loop';
import { Map } from 'immutable';

import weatherReducer from '../containers/RootState';

const reducers = {
  weather: weatherReducer
};

const reducer = combineReducers(
  reducers,
  Map(),
  (child, key) => child.get(key),
  (child, key, value) => child.set(key, value)
);

export default reducer;
