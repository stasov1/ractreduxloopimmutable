import fetch from 'isomorphic-fetch';
import { fromJS } from 'immutable';
import { loop, Effects } from 'redux-loop';

const initialState = fromJS({
  error: false,
  loading: false,
  weather: {}
});

const REQUEST_DATA = 'REQUEST_DATA';
const RESPONSE_DATA = 'RESPONSE_DATA';

export function requestData(city) {
  return {
    type: REQUEST_DATA,
    payload: city
  }
}

async function responseData(city) {
  return {
    type: RESPONSE_DATA,
    payload: await fetchingData(city)
  }
}

async function fetchingData(city) {
  let data;

  try {
    data = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&lang=ru&APPID=b219a99a85f9d404c370fd58a330a851`)
      .then(response => response.json());
  } catch (e) {
    console.error(e);
  }

  return data;
}

export default function weatherReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_DATA:
      return loop(
        state
          .set('loading', true)
          .set('error', false),
        Effects.promise(responseData, action.payload)
      );

    case RESPONSE_DATA:
      return state
        .set('loading', false)
        .set('error', false)
        .set('weather', fromJS(action.payload));

    default:
      return state;
  }
}
