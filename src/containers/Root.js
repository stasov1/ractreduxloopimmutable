import React from 'react';
import { connect } from 'react-redux';

import { requestData } from './RootState';

class Root extends React.Component {
  _getWeather(city) {
    this.props.getData(city);
  }

  render() {
    console.log(this.props);

    return (
      <div>
        <button type="button" onClick={this._getWeather.bind(this, 'Moscow')}>Go!</button>
        {
          this.props.weather.main
            ? <p>Сейчас в Москве {this.props.weather.main && this.props.weather.main.temp}&deg;C</p>
            : ''
        }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    weather: state.getIn(['weather', 'weather']).toJS(),
    loading: state.getIn(['weather', 'loading']),
    error: state.getIn(['weather', 'error'])
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getData(city) {
      return dispatch(requestData(city));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Root);
